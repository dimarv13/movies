package com.dimitrisarvanitis.movies.fragments;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dimitrisarvanitis.movies.MainActivity;
import com.dimitrisarvanitis.movies.R;
import com.dimitrisarvanitis.movies.adapters.SimilarAdapter;
import com.dimitrisarvanitis.movies.databinding.DetailsFBinding;
import com.dimitrisarvanitis.movies.callbacks.RecyclerViewClickListener;
import com.dimitrisarvanitis.movies.models.MainMoviesModel;
import com.dimitrisarvanitis.movies.userPreferences.UserPreferences;
import com.dimitrisarvanitis.movies.viewmodels.MoviesViewModel;

import java.util.List;

public class DetailsFragment extends Fragment {
    DetailsFragment fragment;
    private MainMoviesModel selectedMovie;
    private MoviesViewModel viewModel;
    private List<MainMoviesModel> movieList;
    private DetailsFBinding binding;

    public DetailsFragment getInstance() {
        if (fragment == null)
            fragment = new DetailsFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity()).get(MoviesViewModel.class);
        selectedMovie = viewModel.getSelectedMovie();

        DetailsFBinding binding = DataBindingUtil.inflate(inflater, R.layout.details_f, container, false);
        View v = binding.getRoot();

        setupView(binding);
        return v;
    }

    private void setupView(DetailsFBinding binding) {
        this.binding = binding;
        RequestOptions options = new RequestOptions().centerCrop();
        String posterUrl = viewModel.getBaseUrl() + viewModel.getSelectedMovie().getPoster_path();
        Glide.with(getActivity()).load(posterUrl).apply(options).into(binding.imageView2);
        binding.date.setText(selectedMovie.getMoreReadableDate());
        binding.overview.setText(getString(R.string.description) + selectedMovie.getOverview());
        binding.voteAverage.setText(selectedMovie.getVoteString());
        ((MainActivity) getActivity()).setToolbarTitle(selectedMovie.getTitle());
        binding.toggleButton.setChecked(new UserPreferences().getLiked(String.valueOf(selectedMovie.getId()),getActivity()));
        binding.toggleButton.setOnCheckedChangeListener((buttonView, isChecked) -> new UserPreferences().setLiked(String.valueOf(selectedMovie.getId()), isChecked, getActivity()));
        new LoadAndShowSimilar().execute();

    }

    RecyclerViewClickListener recyclerViewClickListener = (v, position) -> {
      // viewModel.setSelectedMovie(movieList.get(position));
        //getFragmentManager().beginTransaction().add(R.id.fragment, new DetailsFragment().getInstance()).addToBackStack("details").commit();
    };


    @Override
    public void onPause() {
        ((MainActivity) getActivity()).setToolbarTitle(getActivity().getString(R.string.app_name));
        super.onPause();
    }

    private class LoadAndShowSimilar extends AsyncTask<String, String, List<MainMoviesModel>> {
        @Override
        protected List<MainMoviesModel> doInBackground(String... strings) {
            try {
                return viewModel.getSimilarMovieList(selectedMovie.getId());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<MainMoviesModel> mainMoviesModels) {
            super.onPostExecute(mainMoviesModels);
            if (mainMoviesModels == null)
                return;
            movieList = mainMoviesModels;

            SimilarAdapter mAdapter = new SimilarAdapter(movieList, (MainActivity) getActivity(), recyclerViewClickListener);
            binding.recyclerview.setItemAnimator(new DefaultItemAnimator());
            binding.recyclerview.setAdapter(mAdapter);
        }
    }
}
