package com.dimitrisarvanitis.movies.fragments;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import com.dimitrisarvanitis.movies.MainActivity;
import com.dimitrisarvanitis.movies.R;
import com.dimitrisarvanitis.movies.adapters.MoviesAdapter;
import com.dimitrisarvanitis.movies.databinding.MasterFBinding;
import com.dimitrisarvanitis.movies.callbacks.RecyclerOnScrollListener;
import com.dimitrisarvanitis.movies.callbacks.RecyclerViewClickListener;
import com.dimitrisarvanitis.movies.models.MainMoviesModel;
import com.dimitrisarvanitis.movies.viewmodels.MoviesViewModel;

import java.util.ArrayList;
import java.util.List;

public class MasterFragment extends Fragment {
    private MasterFragment fragment;
    private MoviesViewModel viewModel;
    private List<MainMoviesModel> movieList;
    private List<MainMoviesModel> visibleMovieList;
    private RecyclerView recyclerView;
    private MoviesAdapter mAdapter;
    private LayoutAnimationController controller;
    private int n=0;

    public MasterFragment getInstance() {
        if (fragment == null)
            fragment = new MasterFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        viewModel = ViewModelProviders.of(getActivity()).get(MoviesViewModel.class);
        MasterFBinding binding = DataBindingUtil.inflate(inflater, R.layout.master_f, container, false);
        View v = binding.getRoot();
        viewModel.movieList.observe(getActivity(), mainMoviesModels -> {
            movieList = mainMoviesModels;
           visibleMovieList=new ArrayList<>();
            n=0;
            addMoreElements();
        });
         controller =
                AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation);
        recyclerView = binding.recyclerview;
        recyclerView.addOnScrollListener(new RecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
            addMoreElements();
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        recyclerView.scrollToPosition(viewModel.getAdapterPosition());
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        viewModel.setAdapterPosition(((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition());
    }

    RecyclerViewClickListener recyclerViewClickListener = new RecyclerViewClickListener() {
        @Override
        public void recyclerViewListClicked(View v, int position) {
            viewModel.setSelectedMovie(visibleMovieList.get(position));
            getFragmentManager().beginTransaction().add(R.id.fragment, new DetailsFragment().getInstance()).addToBackStack("details").commit();
        }
    };

    private void addMoreElements(){
        int endIndex=(n+10)>movieList.size()? n=movieList.size():n+10;
        visibleMovieList.addAll(movieList.subList(n,endIndex));
        n+=10;
        createAdapter();
    }

    private void createAdapter() {
        recyclerView.setLayoutAnimation(controller);
        recyclerView.scheduleLayoutAnimation();
        if (mAdapter == null) {
            mAdapter = new MoviesAdapter(visibleMovieList, (MainActivity) getActivity(), recyclerViewClickListener);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.updateList(visibleMovieList);
            mAdapter.notifyDataSetChanged();
        }

    }
}
