package com.dimitrisarvanitis.movies;

import android.app.SearchManager;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.dimitrisarvanitis.movies.databinding.ActivityMainBinding;
import com.dimitrisarvanitis.movies.fragments.MasterFragment;
import com.dimitrisarvanitis.movies.viewmodels.MoviesViewModel;

public class MainActivity extends AppCompatActivity {

    private MasterFragment masterFragment;
    private Toolbar toolbar;
    private MoviesViewModel viewModel;
    private SearchView searchView;
    private boolean searchHappened = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(MoviesViewModel.class);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        initToolbar();
        //RoomDB.create(this.getApplicationContext());
        if (savedInstanceState == null) {
            masterFragment = new MasterFragment().getInstance();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment, masterFragment).addToBackStack("master").commit();
        }
    }

    private void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuItem search = menu.findItem(R.id.action_search);
        searchView = (SearchView) search.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        search.setOnActionExpandListener(listener);
        searchView.setOnQueryTextListener(searchListener);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_refresh && getFragmentManager().getBackStackEntryCount() < 2) {
            viewModel.fetchPopularMovies();
        }
        return super.onOptionsItemSelected(item);
    }

    MenuItem.OnActionExpandListener listener = new MenuItem.OnActionExpandListener() {
        @Override
        public boolean onMenuItemActionExpand(MenuItem item) {
            return true;
        }

        @Override
        public boolean onMenuItemActionCollapse(MenuItem item) {
            if (searchHappened) {
                viewModel.fetchPopularMovies();
                searchHappened = false;
            }
            return true;
        }
    };


    SearchView.OnQueryTextListener searchListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            viewModel.fetchSearchResults(query);
            searchHappened = true;
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            if (newText.length() > 2) {
                viewModel.fetchSearchResults(newText);
                searchHappened = true;
            }
            return false;
        }
    };


    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
        } else if (getFragmentManager().getBackStackEntryCount() > 1)
            getFragmentManager().popBackStack();
        else {
            super.onBackPressed();
            finish();
        }
    }

    public void setToolbarTitle(String title) {
        toolbar.setTitle(title);
    }
}

