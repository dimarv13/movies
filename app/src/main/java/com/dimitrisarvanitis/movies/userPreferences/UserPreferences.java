package com.dimitrisarvanitis.movies.userPreferences;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.dimitrisarvanitis.movies.R;

public class UserPreferences {

    private void writeToPrefs(String id, String value, Activity activity) {
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(id, value);
        editor.commit();
    }

    private String readFromPrefs(String id, String defValue, Activity activity) {
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        return sharedPref.getString(id, defValue);
    }


    public void setLiked(String id, boolean value, Activity activity) {
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(id, value);
        editor.commit();
    }

    public boolean getLiked(String id, Activity activity) {
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        return sharedPref.getBoolean(id, false);
    }

}
