package com.dimitrisarvanitis.movies.repositories;

import com.dimitrisarvanitis.movies.models.MainMoviesModel;
import com.dimitrisarvanitis.movies.repositories.BEService.BaseService;
import com.dimitrisarvanitis.movies.repositories.RoomDB.MoviePrefs;
import com.dimitrisarvanitis.movies.repositories.RoomDB.RoomDB;

import java.io.IOException;
import java.util.List;

public class Repository {
    private RoomDB db;

    public List<MainMoviesModel> getMovies() throws IOException {
        return new BaseService().getMovies();
    }

    public List<MainMoviesModel> getSimilarMovies(int id) throws IOException {
        return new BaseService().geSimilartMovies(id);
    }

    public List<MainMoviesModel> getSearchMovies(String param) throws IOException {
        return new BaseService().getSearchResults(param);
    }

    public boolean getMovieWasLiked(int id) {
        return RoomDB.getInstance().getMoviePrefsDao().isPreviouslyLiked(id);
    }

    public void setMovieWasLiked(int id, boolean liked) {
        new Thread(() -> {
            if (liked)
                RoomDB.getInstance().getMoviePrefsDao().movieWasLiked(new MoviePrefs(id, true));
            else
                RoomDB.getInstance().getMoviePrefsDao().movieWasUnliked(new MoviePrefs(id, true));
        }).start();
    }


}
