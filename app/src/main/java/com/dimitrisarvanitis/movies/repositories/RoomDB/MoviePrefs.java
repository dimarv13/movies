package com.dimitrisarvanitis.movies.repositories.RoomDB;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "prefs")
public class MoviePrefs {

    public MoviePrefs(int id,boolean liked){
        this.id=id;
        this.liked=liked;
    }

    @PrimaryKey
    public int id;

    @ColumnInfo(name = "liked")
    public boolean liked;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }
}
