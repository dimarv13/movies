package com.dimitrisarvanitis.movies.repositories.BEService;

import android.util.Log;

import com.dimitrisarvanitis.movies.models.MainMoviesModel;
import com.dimitrisarvanitis.movies.models.PopularMoviesModel;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class BaseService {
    private MovieService service;
    private final static String BASE_URL = "https://api.themoviedb.org/3/";
    private final static String api_key="aab98a8582fed77292642e036ca5aa7b";



    public interface MovieService {
        @GET("movie/popular?api_key")
        Call<PopularMoviesModel> moviesList(@Query("api_key") String key);

        @GET("search/movie?")
        Call<PopularMoviesModel> searchList(@Query("api_key") String key,@Query("query") String param);

        @GET("/3/movie/{movie_id}/similar?")
        Call<PopularMoviesModel> similarMoviesList(@Path("movie_id") int id,@Query("api_key") String key);

    }

    public MovieService getService() {
        if (service == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BASE_URL)
                    .build();

            service = retrofit.create(MovieService.class);
        }
        return service;

    }

    public List<MainMoviesModel> getMovies() throws IOException {
        Call<PopularMoviesModel> req=getService().moviesList(api_key);

        return req.execute().body().getResults();
    }

    public List<MainMoviesModel> geSimilartMovies(int id) throws IOException {
        Call<PopularMoviesModel> req=getService().similarMoviesList(id,api_key);
        Log.d(req.request().toString(),"tag");
        return req.execute().body().getResults();
    }

    public List<MainMoviesModel> getSearchResults(String param) throws IOException {
        Call<PopularMoviesModel> result=getService().searchList(api_key,param);
        List<MainMoviesModel> list= result.execute().body().getResults();
    return list;
    }



}
