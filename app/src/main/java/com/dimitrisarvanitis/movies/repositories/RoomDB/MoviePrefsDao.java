package com.dimitrisarvanitis.movies.repositories.RoomDB;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface MoviePrefsDao {

@Query("SELECT * FROM prefs WHERE id = :id")
    boolean isPreviouslyLiked(int id);

@Insert
    void movieWasLiked(MoviePrefs moviePrefs);

@Delete
    void movieWasUnliked(MoviePrefs moviePrefs);
}
