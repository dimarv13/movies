package com.dimitrisarvanitis.movies.repositories.RoomDB;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import android.content.Context;

@Database(entities = {MoviePrefs.class}, version = 1)
public abstract class RoomDB extends RoomDatabase {
    private static final String DB_NAME = "room.db";
    private static volatile RoomDB instance;
    private static Context ctx;

    public static synchronized RoomDB getInstance() {
        if (instance == null) {
            instance = create(ctx);
        }
        return instance;
    }

    public static RoomDB create(final Context context) {
        ctx=context;
        return Room.databaseBuilder(
                context,
                RoomDB.class,
                DB_NAME).build();
    }



    public abstract MoviePrefsDao getMoviePrefsDao();

}
