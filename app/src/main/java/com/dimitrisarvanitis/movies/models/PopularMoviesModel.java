package com.dimitrisarvanitis.movies.models;

import java.util.List;

public class PopularMoviesModel {
    private int page;
    private List<MainMoviesModel> results;

    public List<MainMoviesModel> getResults() {
        return results;
    }

    public int getPage() {
        return page;
    }
}
