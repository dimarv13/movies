package com.dimitrisarvanitis.movies.models;

import android.icu.text.SimpleDateFormat;

import java.io.Serializable;

public class MainMoviesModel implements Serializable {

    private String title;
    private String poster_path;
    private String release_date;
    private boolean liked;
    private int id;
    private String overview;
    private float vote_average;

    public float getVote_average() {
        return vote_average;
    }

    public String getVoteString(){
        return String.valueOf(vote_average);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public int getId() {
        return id;
    }

    public String getOverview() {
        return overview;
    }

    public String getMoreReadableDate(){
        SimpleDateFormat fromServer = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat myFormat = new SimpleDateFormat("dd-MMMM-yyyy");

        try {

            return myFormat.format(fromServer.parse(release_date));
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return release_date;
    }
}
