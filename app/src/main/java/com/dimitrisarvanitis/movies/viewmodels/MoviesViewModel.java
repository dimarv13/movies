package com.dimitrisarvanitis.movies.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import android.os.AsyncTask;
import androidx.annotation.NonNull;

import com.dimitrisarvanitis.movies.models.MainMoviesModel;
import com.dimitrisarvanitis.movies.repositories.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MoviesViewModel extends AndroidViewModel {


    private MainMoviesModel selectedMovie;
    private int adapterPosition;

    public MutableLiveData<List<MainMoviesModel>> movieList = new MutableLiveData<>();
    private final static String BASE_URL = "https://image.tmdb.org/t/p/original";

    @NonNull
    @Override
    public <T extends Application> T getApplication() {
        return super.getApplication();
    }

    public MoviesViewModel(@NonNull Application application) {
        super(application);
        fetchPopularMovies();
    }


    public MutableLiveData<List<MainMoviesModel>> getMovies() {
        return movieList;
    }

    public List<MainMoviesModel> getSimilarMovieList(int id) throws IOException {
        return new Repository().getSimilarMovies(id);
    }

    public MainMoviesModel getSelectedMovie() {
        return selectedMovie;
    }


    public void setSelectedMovie(MainMoviesModel selectedMovie) {
        this.selectedMovie = selectedMovie;
    }

    public void setMovieList(List<MainMoviesModel> movieList) {
        this.movieList.setValue(movieList);
    }

    public void fetchPopularMovies() {
        new Load().execute();
    }

    public void fetchSearchResults(String param) {
        String[] strings = new String[1];
        strings[0] = param;
        new Load().execute(strings);
    }

    public String getBaseUrl() {
        return BASE_URL;
    }

    private class Load extends AsyncTask<String, String, List<MainMoviesModel>> {
        @Override
        protected List<MainMoviesModel> doInBackground(String... strings) {
            List<MainMoviesModel> list = new ArrayList<>();
            try {
                if (strings.length == 0) {
                    list = new Repository().getMovies();
                } else {
                    list = new Repository().getSearchMovies(strings[0]);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return list;
        }

        @Override
        protected void onPostExecute(List<MainMoviesModel> mainMoviesModels) {
            super.onPostExecute(mainMoviesModels);
            setMovieList(mainMoviesModels);
        }
    }

    public int getAdapterPosition() {
        return adapterPosition;
    }

    public void setAdapterPosition(int adapterPosition) {
        this.adapterPosition = adapterPosition;
    }
}
