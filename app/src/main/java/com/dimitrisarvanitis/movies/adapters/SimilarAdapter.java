package com.dimitrisarvanitis.movies.adapters;

import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dimitrisarvanitis.movies.MainActivity;
import com.dimitrisarvanitis.movies.R;
import com.dimitrisarvanitis.movies.callbacks.RecyclerViewClickListener;
import com.dimitrisarvanitis.movies.models.MainMoviesModel;

import java.util.List;

public class SimilarAdapter extends RecyclerView.Adapter<SimilarAdapter.ItemViewHolder> {

    private final static String BASE_URL = "https://image.tmdb.org/t/p/w500";

    private MainActivity context;
    private static RecyclerViewClickListener itemListener;
    private List<MainMoviesModel> movieList;
    private MainMoviesModel movie;
    private View view;

    public SimilarAdapter(List<MainMoviesModel> movieList, MainActivity context, RecyclerViewClickListener itemListener) {
        this.context = context;
        this.itemListener = itemListener;
        this.movieList = movieList;
    }


    @NonNull
    @Override
    public SimilarAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.similar_item, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        movie = movieList.get(position);
        String posterUrl = BASE_URL + movie.getPoster_path();
        Glide.with(context).load(Uri.parse(posterUrl)).apply( new RequestOptions().centerCrop()).into(holder.img);
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }



    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView img;


        public ItemViewHolder(View convertView) {
            super(convertView);
            img = convertView.findViewById(R.id.similar_iv);
            convertView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
           // itemListener.recyclerViewListClicked(v, this.getLayoutPosition());

        }
    }
}
