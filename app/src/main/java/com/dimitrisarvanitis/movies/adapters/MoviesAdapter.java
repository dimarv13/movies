package com.dimitrisarvanitis.movies.adapters;

import androidx.lifecycle.MutableLiveData;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.dimitrisarvanitis.movies.MainActivity;
import com.dimitrisarvanitis.movies.R;
import com.dimitrisarvanitis.movies.databinding.MovieItemBinding;
import com.dimitrisarvanitis.movies.callbacks.RecyclerViewClickListener;
import com.dimitrisarvanitis.movies.models.MainMoviesModel;
import com.dimitrisarvanitis.movies.userPreferences.UserPreferences;

import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.ItemViewHolder> {

    private final static String BASE_URL = "https://image.tmdb.org/t/p/w500";

    private MainActivity context;
    private RecyclerViewClickListener itemListener;
    private List<MainMoviesModel> movieList;
    private MainMoviesModel movie;
    private MutableLiveData<Boolean> wasLiked = new MutableLiveData<>();
    private UserPreferences prefs = new UserPreferences();

    public MoviesAdapter(List<MainMoviesModel> movieList, MainActivity context, RecyclerViewClickListener itemListener) {
        this.context = context;
        this.itemListener = itemListener;
        this.movieList = movieList;
    }

    public void updateList(List<MainMoviesModel> movieList) {
        //DiffUtil.DiffResult diffResultlt = DiffUtil.calculateDiff(new MyDiffUtilCallBack(movieList, this.movieList));
        this.movieList = movieList;
        //diffResultlt.dispatchUpdatesTo(this);
    }


    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());
        MovieItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.movie_item, parent, false);
        return new ItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        movie = movieList.get(position);

        holder.binding.title.setText(movie.getTitle());
        holder.binding.date.setText(movie.getMoreReadableDate());
        holder.binding.liked.setOnCheckedChangeListener(null);
        holder.binding.liked.setChecked(prefs.getLiked(String.valueOf(movie.getId()), context));
        holder.binding.liked.setTag(movie.getId());
        holder.binding.liked.setOnCheckedChangeListener((buttonView, isChecked) -> prefs.setLiked(String.valueOf(buttonView.getTag()), isChecked, context));
        String posterUrl = BASE_URL + movie.getPoster_path();
        Glide.with(context).load(Uri.parse(posterUrl)).into(holder.binding.imageView);
    }


    @Override
    public int getItemCount() {
        return movieList.size();
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private MovieItemBinding binding;

        ItemViewHolder(MovieItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, this.getLayoutPosition());

        }
    }

}
