package com.dimitrisarvanitis.movies.callbacks;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import com.dimitrisarvanitis.movies.models.MainMoviesModel;

import java.util.List;

public class MyDiffUtilCallBack extends DiffUtil.Callback {
    List<MainMoviesModel> oldList;
    List<MainMoviesModel> newList;

    public MyDiffUtilCallBack(List<MainMoviesModel> newList, List<MainMoviesModel> oldList) {
        this.newList = newList;
        this.oldList = oldList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).getId() == newList.get(newItemPosition).getId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).equals(newList.get(newItemPosition));
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        //you can return particular field for changed item.
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}
